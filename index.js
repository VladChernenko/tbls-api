import * as dkg from './tbls_index.js'
import blsA from 'bls-wasm'

await blsA.init()

export default {

    // Функція генерації часткових шар та ID
    generateTBLS:(threshold,myPubId,pubKeysArr)=>{

        let signers=pubKeysArr.map(id => {

            const sk = new blsA.SecretKey()
        
            sk.setHashOf(Buffer.from([id]))
        
            return {id:sk,recievedShares:[],arrId:id}
        
        })

        
        //Ось процес генерації для учасників – вони можуть це робити приватно у себе
        //Generation process - signers can do it privately on theirs machines
        const {verificationVector,secretKeyContribution} = dkg.generateContribution(blsA,signers.map(x=>x.id),threshold)
      
        
        //Проводимо серіалізацію для того, щоб легше було експортувати(в файл, через мережу тощо)
        //Verification vector можемо публікувати - для кожного в групі. Але варто запам'ятати порядок індексів

        let serializedVerificationVector=verificationVector.map(x=>x.serializeToHexStr())
        let serializedSecretKeyContribution=secretKeyContribution.map(x=>x.serializeToHexStr())

        let jsonVerificationVector=JSON.stringify(serializedVerificationVector)

        let serializedId=signers[pubKeysArr.indexOf(myPubId)].id.serializeToHexStr()


        // Виведемо результати з поясненням що куди відправляти, а що варто зберігати в таємниці
        console.log(`Send this verification vector to all group members => ${jsonVerificationVector}`)
        console.log(`\n\nSend this secret shares to appropriate user(one per user)`)
        
        // Тут цикл для окремого виведення по учасникам
        serializedSecretKeyContribution.forEach((share,index)=>{

            console.log(`To user ${index} => ${share}`)

        })

        console.log(`\n\nYour ID ${serializedId}`)


        //Також, повернемо ці дані якщо працюємо з кодом(і консоль нам непотрібна)
        return JSON.stringify({
        
            verificationVector:serializedVerificationVector,
            secretShares:serializedSecretKeyContribution,
            id:serializedId
        
        })

    },

    //Функція перевірки шари яку ми отримаємо від друзів. Необхідно перевіряти кожну шару, в іншому випадку ми ризикуємо втратити доступ до гаманця
    verifyShareTBLS:(hexMyId,hexSomeSignerSecretKeyContribution,hexSomeSignerVerificationVector)=>{
        
        //____________________________Десеріалізуємо з hex формату____________________________

        // Шара в hex форматі отримана від деякої іншої сторони(в нашому випадку - це шара яку ви отримаєте від одного з друзів)
        let someSignerSecretKeyContribution=blsA.deserializeHexStrToSecretKey(hexSomeSignerSecretKeyContribution)
        
        // Вектор верифіцацї отриманий від сторони X
        let someSignerVerificationVector=hexSomeSignerVerificationVector.map(x=>blsA.deserializeHexStrToPublicKey(x))
        
        // Наш ID отриманий на етапі нашої генерації(дивись попередню функцію)
        let myId = blsA.deserializeHexStrToSecretKey(hexMyId)
    

        // Тепер коли потрібний член групи підписантів отримав цю шару, то він перевіряє його VSS властивості використовуючи вектор верифікації(verification vector)
        // Теперь когда нужный член групы получил этот secret sk,то он проверяет его по VSS с помощью  of the sender и сохраняет его если всё ок
        const isVerified = dkg.verifyContributionShare(blsA,myId,someSignerSecretKeyContribution,someSignerVerificationVector)
     
        // Якщо на даному етапі виявиться, що шара невалідна - то зупиніться !!! Це означає, що ця сторона хоче вас обманути підсунувши невалідну шару
        if(!isVerified) console.log(`\n\n\x1b[31mInvalid\x1b[0m share received from user with verification vector ${hexSomeSignerVerificationVector}`)
        
        // Якщо ж все добре - то зберігайте цю шару локально. Її не можна нікому демонструвати. Її знає тільки той хто її генерував(одна зі сторін) та ви
        else console.log(`\n\nShare ${hexSomeSignerSecretKeyContribution} is \x1b[32mvalid\x1b[0m - please,store it`) 
     
    
        return isVerified
    
    },    


    
    
    /**
     *   ## Функція для одержання публічного ключа групи(так званий MasterPub). Ми отримуємо його з векторів верифікації кожної зі сторін 
     *
     *   @param {Array<Array<string>>} hexVerificationVectors масив серіалізоавних у hex форматі векторів верифікації. Наприклад, [ [hex1,hex2], [hex3,hex4], ...] де [hexA,hexB] - деякий вектор верифікації 
     * 
     */
    deriveGroupPubTBLS:hexVerificationVectors=>{

        console.log(hexVerificationVectors.map(subArr=>

            subArr.map(x=>blsA.deserializeHexStrToPublicKey(x))

        ))

        // Групуємо
        const groupVvec = dkg.addVerificationVectors(hexVerificationVectors.map(subArr=>

            subArr.map(x=>blsA.deserializeHexStrToPublicKey(x))

        ))
        
        // Серіалізуємо для одержання MasterPub у hex форматі
        const groupPublicKey = groupVvec[0].serializeToHexStr()

        console.log(`Group TBLS pubKey is ${groupPublicKey}`)
        
        //blsA.deserializeHexStrToPublicKey(groupsPublicKey.serializeToHexStr())// - для десеріалізації

        return groupPublicKey

    },



    /*

    Функція для генерації часткового підпису - так звані SigShare
    Цю функцію викликають T з N учасників-підписантів коли хочуть підтвердити своє рішення


    На вхід поступють дані виду

    {

        hexMyId - id отриманий із generateTBLS
        
        sharedPayload:[
            {
                verificationVector://Вектор верифікації отриманий від підписанта_1 у hex форматі
                secretKeyShare://Шара яку ми отримали від підписанта_1 у hex форматі
            },
            {
                verificationVector://Вектор верифікації отриманий від підписанта_2 у hex форматі
                secretKeyShare://Шара яку ми отримали від підписанта_2 у hex форматі
            },
            ...,
            {
                
                verificationVector://Вектор верифікації отриманий від підписанта_N у hex форматі
                secretKeyShare://Шара яку ми отримали від підписанта_N у hex форматі

            }
        ]

        message - повідомлення яке ми хочемо підписати.

    }

*/
    signTBLS:(hexMyId,sharedPayload,message)=>{

        //Отримуємо значення групового секрету з шар отриманих раніше від інших підписантів
        let groupSecret=dkg.addContributionShares(

            sharedPayload

                .map(x=>x.secretKeyShare)
                
                .map(hexValue=>blsA.deserializeHexStrToSecretKey(hexValue))

        )

        // Виведемо це значення
        console.log(`\n\nDerived group secret ${groupSecret.serializeToHexStr()}`)

        // Повернемо значення підпису, знову ж таки в hex форматі
        return JSON.stringify({sigShare:groupSecret.sign(message).serializeToHexStr(),id:hexMyId})

    },


    
    /*

        Функція для побудови MasterSig з отриманих SigShares інших підписантів.
        Короче кажучи, кожний з T підписантів викликає у себе функцію .signTBLS() і результат поширює серед інших. Цей результат - це частковий підпис(SigShare)

        В рамках цієї функції, ми збираємо ці T часткових підписів та формуємо з них повний підпис який можна буде перевірити використовуючи MasterPub отриманий з функції .deriveGroupPubTBLS()


        Формат параметрів:

            signaturesArray - [ {sigShare:signedShareA,id:hexIdA}, {sigShare:signedShareB,id:hexIdB},... {sigShare:signedShareX,id:hexIdX} ]

            [+] sigShareN - частковий підпис від учасника N в hex форматі
            [+] id - ID цього учасника


    */
    buildSignature:signaturesArray=>{

        //Конструктор
        const groupsSig = new blsA.Signature()

        let sigs=[],signersIds=[]

        // Проходимось по парах та десеріалізуємо (hex=>bytes array)
        signaturesArray.forEach(x=>{

            sigs.push(blsA.deserializeHexStrToSignature(x.sigShare))

            signersIds.push(blsA.deserializeHexStrToSecretKey(x.id))

        })

        // Отримуємо значення MasterSig
        groupsSig.recover(sigs,signersIds)

        console.log('Signature', groupsSig.serializeToHexStr())
    
        //blsA.deserializeHexStrToSignature(groupsSig.serializeToHexStr())

        return groupsSig.serializeToHexStr()

    },

    /*
    
        Ну і власне функція верифікації

        [+] hexGroupPubKey - це MasterPub отриманий з функції .deriveGroupPubTBLS() на основі векторів верифікації сторін підписантів
        [+] hexSignature - це MasterSig отриманий з функції .buildSignature() на основі часткових підписів сторін підписантів
        [+] signedMessage - це власне повідомлення яке хочуть підписати T/N учасників
    
    */
    verifyTBLS:(hexGroupPubKey,hexSignature,signedMessage)=>{


        let groupPubKey=blsA.deserializeHexStrToPublicKey(hexGroupPubKey),

            verified=groupPubKey.verify(blsA.deserializeHexStrToSignature(hexSignature),signedMessage)


        return verified

    }

}