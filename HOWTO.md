# <b>Приклад користування API в реальних проектах</b>

Для того, щоб уникнути непорозумінь та неоднозначностей при використанні модуля, створимо тестовий приклад де можна буде чітко побачити процес користування підписами, а також розібратися що і в якому порядку генерувати, відправляти тощо.

Для цього використаємо консоль.


## <b>Ситуація</b>

Є 6 друзів які хочуть спільно використовувати деяку криптовалюту коли поїдуть на фестиваль. При цьому, була домовленість, що витрати погоджуються із більшістю. Тобто, 4 із 6 друзів мають погодитись з певною покупкою чи іншим видом витрат. Для цього, друзі домовились, що кожний з них поповнить спільний гаманець на 500 одиниць цієї криптовалюти. Що ж, давайте допоможемо їм згенерувати такий гаманець, а також все інше для чесного керування грошима.


### <b>Етап 1 - генерація</b>

Отже, розглянемо сигнатуру першої функції <code>generateTBLS()</code>

Вона має такі параметри:


<ul>
    <li>
    <code>threshold</code> - поріг. В нашому випадку буде рівний 4
    </li>
    <li>
    <code>myPubId</code> - обираємо деякий початковий числовий ID для себе
    </li>
    <li>
    <code>pubKeysArr</code> - масив інших ідентифікаторів. Наш ідентифікатор також має бути включеним в цей масив
    </li>
</ul>

Давайте генерувати


```js

let threshold = 4 // 4/6

let myPubId = 1 //наш ID

let pubKeysArr = [1,2,3,4,5,6] //масив ідентифікаторів інших учасників


let {verificationVector,secretShares,id} = module.generateTBLS(threshold,myPubId,pubKeysArr)


console.log(verificationVector)
/*

[
    
    "a7e972de2362e725831efdebe080842f9ee275915a718dea43e561fa0d1af41b3192f849e1128d69f62018b9e7a9387f8b8611488a72480305991d480dad0e11"
    "0003e06c68ba56e45db0ee64a06a6935554e0b7f4849da26223cf831cdfd341bf9fe628590eb436197145206eb8f2eb0baef673eff23f63eaa6fe74e59da7c05"
    "53ad69e15e782a60fe63ee07d9e768dff002dfc54fa6fd6e4ea4531fc00d2700472fb244c9fa740ec354e417e8bd241c875caf29c4abad251a042d7becca4d15"
    "21b090515bf15d9ec6c4f4dd4df5f49a64f21e9b9c268b91b09bd77adcf2d517303fe3cf3bdd710ab783ae413476f7ccd050c1be02524a42484243b47767b71f"
    
]

*/


console.log(secretShares)
/*

[
    "79b359142147dd33383e32ac2476577d7760245a644fdad8730b22fad5726b0d"
    "2ba86e244fe8ad6ea665598257e1a8700b433a2e5bd967116fcf982b9db0df05"
    "af5f947ef9db70f50a8a5a5ee07170e78974c641b52942681a229605ff860e1f"
    "0f0389cb5fdc97c14d3c494ada022bafb78988efb891258b9c487b9475febd0a"
    "c8472e66991a8a1121a5f89f3ae5c1200455f77da838b965f8b9ef5af0701e07"
    "565a252bc870bdcf046fb76a25c45c56cab9d4a5624e794319dd13dbc7e9781f"
]

*/


console.log(id)

//4bf5122f344554c53bde2ebb8cd2b7e3d1600ad631c385a5d7cce23c7785451a

```

Круто, тепер розберемося що ми маємо:

<ul>
    <li>
    <code>verificationVector</code> - це вектор верифікації який ми маємо відправити всім іншим 5 друзям
    </li>
    <li>
    <code>secretShares</code> - це шари які ми маємо ПО ОДНІЙ відправити друзям. Тобто, перша шара(secretShares[0]) йде до друга з ID=1 (це ми), друга шара(secretShares[1]) йде до другого друга - той який з ID=2 і так далі. Тут їх 6,тому зрозуміло, що кожному по шарі.
    </li>
    <li>
    <code>id</code> - це наш ID який ми тепер будемо використовувати для подальших генерацій
    </li>
</ul>

### <b>Етап 2 - друзі роблять те саме</b>

Аналогічну процедуру роблять локально у себе і 5 ваших друзів. Кожний з них отримає свій ID, кожний отримає масив із 6 шар та вектор верифікації.

> Важливий момент - погодьте з друзями набір ідентифікаторів

Мається на увазі, що інші 6 друзів також мають використовувати той же масив ідентифікаторів. І для всіх друзів ваш ID буде однаковий. Тобто:

Для друга з ID=1:

<ul>

<li>Бере для себе шару 1</li>

</ul>


Для друга з ID=2:

<ul>

<li>Бере для себе шару 2</li>

</ul>

І так далі.


### <b>Етап 3 - одержання загального публічного ключа</b>

На цьому етапі друзі отримають деякий адрес в мережі криптовалюти на який вони зможуть відправити кошти для спільного користування. Для цього вони обмінюються векторами верифікацій. Коли ви отримаєте від 5 інших друзів 5 векторів верифікації, то додаєте свій шостий і викликаєте функцію <code>deriveGroupPubTBLS()</code>.

Але перш ніж розібрати, давайте створимо деякий уявний датасет з даними інших 5 друзів. Отже, нехай він матиме такий вигляд


```js


/*

------------------------------- ДРУГ 1 -------------------------------


Вектор верифікації

[
    
    "a7e972de2362e725831efdebe080842f9ee275915a718dea43e561fa0d1af41b3192f849e1128d69f62018b9e7a9387f8b8611488a72480305991d480dad0e11"
    "0003e06c68ba56e45db0ee64a06a6935554e0b7f4849da26223cf831cdfd341bf9fe628590eb436197145206eb8f2eb0baef673eff23f63eaa6fe74e59da7c05"
    "53ad69e15e782a60fe63ee07d9e768dff002dfc54fa6fd6e4ea4531fc00d2700472fb244c9fa740ec354e417e8bd241c875caf29c4abad251a042d7becca4d15"
    "21b090515bf15d9ec6c4f4dd4df5f49a64f21e9b9c268b91b09bd77adcf2d517303fe3cf3bdd710ab783ae413476f7ccd050c1be02524a42484243b47767b71f"
    
]


Набір шар

[
    "79b359142147dd33383e32ac2476577d7760245a644fdad8730b22fad5726b0d"      залишається у нас
    "2ba86e244fe8ad6ea665598257e1a8700b433a2e5bd967116fcf982b9db0df05"      йде до друга 2
    "af5f947ef9db70f50a8a5a5ee07170e78974c641b52942681a229605ff860e1f"      йде до друга 3
    "0f0389cb5fdc97c14d3c494ada022bafb78988efb891258b9c487b9475febd0a"      йде до друга 4
    "c8472e66991a8a1121a5f89f3ae5c1200455f77da838b965f8b9ef5af0701e07"      йде до друга 5
    "565a252bc870bdcf046fb76a25c45c56cab9d4a5624e794319dd13dbc7e9781f"      йде до друга 6
]

ID = 4bf5122f344554c53bde2ebb8cd2b7e3d1600ad631c385a5d7cce23c7785451a


------------------------------- ДРУГ 2 -------------------------------

Вектор верифікації

[
    
    "6f4c90be986832a71aa5b8d25007d27adc3aa267acdca189a42b9a17efc8dd17bcde25c173d18eeba5bae067e52cc5ba16fd3099af04804c8a40b79ac595c617"
    "775449cde76ce6333c53317e6b6a1eb47ee87af2bc56548bb38fea5da266dd0704ae043c288f9fb792b870128738ad7360454724325b5260bb45e332a3d51221"
    "dd5a9d1f3236dd7e091dcb4272fb3cec624c22a0e59de283ce345603850e180e263806875158263b2bd46f0b7af4d78596e43008948431b07d174b6c8d6da388"
    "8176bdd4610dd21cdb6bfb9b526a9e3ee2804c57ac9b9094235a4c1947c2f20aa1282cc8dd5e2ec09183c677920106024aab0bd9a2ecaa5539ff9e1c4025d1a3"
]

Набір шар

[

    173fc58cb61188a8fad233a689baec3db34657c3bb8536b21cbad70db463130b        йде до друга 1
    834ffe5b7794a5c8666210a98beaba4a48d344dd2e691ce4edb2196f6b2da317        залишається у нас
    a8e50fdc4d6f1f20047b7297ff54ac2d807cbfb6cd12e8f499e6eaf7b5feb010        йде до друга 3
    50b2c925a3463136414715d0f99c594c1aa2d7e5857d7881f62dcdc7676c191a        йде до друга 4
    d7f329f64448ddd56350a2a3a45d944679a2c4fc76ecb3770d760592b08c8b0a        йде до друга 5
    2ddedf940ea38c9809735793cec9ded6d0d6e573181611c0f3efa604b6b92e1c        йде до друга 6

]


ID=dbc1b4c900ffe48d575b5da5c638040125f65db0fe3e24494b76ea986457d906


------------------------------- ДРУГ 3 -------------------------------

Вектор верифікації

[
    "586797df712ba4dcb15e78a36e6ef4a3f9ba1b05ce3a6a9001f0cdfefbd8f8080a33a3182a9204657594ebc6749656166f119ab2a81515266f62ce10e327ca09"
    "62b9b08eadbbb20ef840923c89ac53b73aa31fa3e493097f49496693a795131b46dba854f76bfaf369beb77a7a80e2fe66525b16341a88022e1dc635bd7dc385"
    "2577181e0384bc99ceb066acf69323e236160796df37f6e688d3274e4e026a03997460021b39da01a8a1465566e08f6a455333742fb6646f11beeab2a8dd2420"
    "b2a2ace162f22b200c7aa807083bae80e69214dcdaa9708c66f81d1e8d4a0c18c3b459214292bd683c9b80809ea3e7e95288656caf0b34ec260b820a79b08520"
]

Набір шар

[

    82858da32c368bc8dea20ab9646d87f84eb478e51a45a55394710c96c71a080f        йде до друга 1
    6a12a6abfe939015aca70e67c96e9768ad9af0e6541c62e82b51ca32176c8816        йде до друга 2
    b607ae0d7be4bbbad607967c40841e67f8b992e15a44fc3d0dbc5a14a16f9b0e        залишається у нас
    73d57dfb5cd773a19a34052184d1f0b2576bc127ee1d92ae596c51bf3a6e2c24        йде до друга 4
    d9dc10623e261047a0ac924595c356f5d9b192ab28f29276bf5c21206c143902        йде до друга 5
    952e03949438a001aafb2b1e864635c25b229047fc85b3178dd1d049ecbdcd04        йде до друга 6

]


ID=084fed08b978af4d7d196a7446a86b58009e636b611db16211b65a9aadff2905

------------------------------- ДРУГ 4 -------------------------------

Вектор верифікації

[
    "7a4a81ba14f4b32b3c7f061f718cc15b0ca3ab76196a7e9e62f70dcc0add10092906a237d27b034f541731df41fee38d2f4b5d8998678b7d06cad2532fbecc13"
    "6edfa6a70925257e952f161d941092bf30cd70d1f27a6cba53affbe9de7a450dfc02b5c500fbeb9ca2443df0a941269ad65c8bede560f033ae802d6499281a9e"
    "492a5db1fdf4f133e4d8e92c0637ae13a8da6f19bc93da4ed6b0736d8bc602063a229846a43e18b8ba39de1cdba274a11fcda3cf482e3ca908cdf3c3f7ffdb04"
    "8396e4d78a12580388c919b8a201036d48a1470c9684eb653ea89f458927730bc32e8f91763dfce581ff60125664cd6e1a20ed684677a6d8dccbb6b23c3a0182"
]

Набір шар

[

    89b3400f482b4bed1e1eba002df97c8a7933c435fdd990d043fd527255010406        йде до друга 1
    36ae6a22edc96b63ae6bf5ca78445f62117d2aeb0606876b5b0cd321c5338b0a        йде до друга 2
    2170ec403427f94086f4e4712aef6e0b719f58160d8acdd27e26eaaa95676e1a        йде до друга 3
    88e7e3aea7506953618a4f6c3b09e52fa7e1f881f16c70b66ef67c2ba80c5c0e        залишається у нас
    3569f445bbc8274318a0a3e761c016610bcb8b2cb414b49a76979181f9a3480f        йде до друга 5
    b7a2bc301f42f0aa7acff13015b5c28ddd13d8b3fee5f3f81978463bba5efb16        йде до друга 6

]

ID=e52d9c508c502347344d8c07ad91cbd6068afc75ff6292f062a09ca381c89e11



------------------------------- ДРУГ 5 -------------------------------


Вектор верифікації

[

    "16ac0490dc79eef786f88dc09c3a8fed15d371e745bdadd9c1f7a08b4d37d50aea565d857cad2b0706642f58caa09fc1073848491994f690b79e8076beca1018"
    "cad2ef53a9dfe02a92606a34f40868fd746209a4ede0c790e0d3ec5e4c90021019b65c7550174fc117aed8d0aa619b94ddd1c08b7a573b9810490e3bbc36ba01"
    "d88949fae2178d3e601abd22f47f64dfd9f8a532d6abd57b3957f0197480cc1b6effc65b04b475279c8de4c585ad8ba518cf35df406fdca8e5f51d5a6918360b"
    "019c9a96b62afb4210616244cdc42936b503814762132658a20c283b9dfe3d005c1c7415903ed953c71539961e911602c8a272d41d5d08184a5f269ee08a941e"
    
]


Набір шар

[

    f441e4786e7501271994d3819ff2997456c281c8de7225f856165fdf61d1270c        йде до друга 1
    3846038253b8099f82f8fa23632a2f41d6dbf1b41a579e59115f3a3d44a9d218        йде до друга 2
    fbffb23b9b08052c57b157908f8eddc61ddec2810d984e79114fac3db8f78017        йде до друга 3
    ba0d2e862b32e36e92a879519bc75a478305a26d292e69c00c5caaa00b4a6f1d        йде до друга 4
    f1271c0a82ef0173a8584b61b9991428ed5aa4e1052318827cd13bce3e076b1b        залишається у нас
    455c84ebeb83ca3ef8c71d09bc310083848ca694244997dbbff4a74af3745a09        йде до друга 6

]

ID=e77b9a9ae9e30b0dbdb6f510a264ef9de781501d7b6b92ae89eb059c5ab7431b



------------------------------- ДРУГ 6 -------------------------------


Вектор верифікації

[
    "76e014c5dc7a2ae9d0271936ecac410bee3baf9b88b64ad6a166a1c2aab0991992fec4dc4a80447cb795fa07cd77cdce1c5680710ba016a041c6db4a782f368d"
    "117b180362500cdbd655d3bb0f97b22b57521302938ce5a34e4bcf3c2988ad10688116b0bedd6215aae314bfb240158834b87bd828c01454916d5066d1241691"
    "2c811310b69511e286bd898a6d0f5a306594357e4faee25e69b76cc21b74e5118d96a26e59263dbfe7819be40d7f404d57dca16c9c17ba3c0f95cd531c133593"
    "93d15991f4f0269886cc3e1b99bf2b79bba44ecdc00891bc4b8e6195e1b14e1557ddf84001ef06d5cc0a3f19502f320bfc6f7e11ed75eb21731f68629897461b"
]

Набір шар

[

    bbf907b9028b87ba335e274f0cbcad8286ea57361788859f6392aef9ea5eb124        йде до друга 1
    f7e2749b4803c55d42d4487916569510e50350d0bb5ec1879d8d00135cc7ae00        йде до друга 2
    1640c3244249f9b215a23015c5ac77bf27a1e9c39b46ca15908646742066be08        йде до друга 3
    aee5fb00cfaedb98c1a6846c3a25c183b57ae1938b0de18fd37503b93b982300        йде до друга 4
    2c84ae3f4fd9d4b8eb3c60e26b248086ff66892cda1a4fa5834cedfebee5cc0c        йде до друга 5
    00f055619d846105cb5de592f1b4f9b209fd8db6b29bf22fd6859dac91102322        залишається у нас

]

ID=67586e98fad27da0b9968bc039a1ef34c939b9b8e523a8bef89d478608c5ec16



*/

```

Викликаємо функцію генерації публічного ключа нашої групи. В даному випадку - ототожнюємо поняття публічного ключа/мережевого адреса. Адрес в будь якому випадку можна отримати з публічного ключа.

Отже,маємо:

```js

let vv1 = 
[
    
    "a7e972de2362e725831efdebe080842f9ee275915a718dea43e561fa0d1af41b3192f849e1128d69f62018b9e7a9387f8b8611488a72480305991d480dad0e11",
    "0003e06c68ba56e45db0ee64a06a6935554e0b7f4849da26223cf831cdfd341bf9fe628590eb436197145206eb8f2eb0baef673eff23f63eaa6fe74e59da7c05",
    "53ad69e15e782a60fe63ee07d9e768dff002dfc54fa6fd6e4ea4531fc00d2700472fb244c9fa740ec354e417e8bd241c875caf29c4abad251a042d7becca4d15",
    "21b090515bf15d9ec6c4f4dd4df5f49a64f21e9b9c268b91b09bd77adcf2d517303fe3cf3bdd710ab783ae413476f7ccd050c1be02524a42484243b47767b71f"
]

let vv2 = 
[
    
    "6f4c90be986832a71aa5b8d25007d27adc3aa267acdca189a42b9a17efc8dd17bcde25c173d18eeba5bae067e52cc5ba16fd3099af04804c8a40b79ac595c617",
    "775449cde76ce6333c53317e6b6a1eb47ee87af2bc56548bb38fea5da266dd0704ae043c288f9fb792b870128738ad7360454724325b5260bb45e332a3d51221",
    "dd5a9d1f3236dd7e091dcb4272fb3cec624c22a0e59de283ce345603850e180e263806875158263b2bd46f0b7af4d78596e43008948431b07d174b6c8d6da388",
    "8176bdd4610dd21cdb6bfb9b526a9e3ee2804c57ac9b9094235a4c1947c2f20aa1282cc8dd5e2ec09183c677920106024aab0bd9a2ecaa5539ff9e1c4025d1a3"
]

let vv3 = 
[
    "586797df712ba4dcb15e78a36e6ef4a3f9ba1b05ce3a6a9001f0cdfefbd8f8080a33a3182a9204657594ebc6749656166f119ab2a81515266f62ce10e327ca09",
    "62b9b08eadbbb20ef840923c89ac53b73aa31fa3e493097f49496693a795131b46dba854f76bfaf369beb77a7a80e2fe66525b16341a88022e1dc635bd7dc385",
    "2577181e0384bc99ceb066acf69323e236160796df37f6e688d3274e4e026a03997460021b39da01a8a1465566e08f6a455333742fb6646f11beeab2a8dd2420",
    "b2a2ace162f22b200c7aa807083bae80e69214dcdaa9708c66f81d1e8d4a0c18c3b459214292bd683c9b80809ea3e7e95288656caf0b34ec260b820a79b08520"
]


let vv4 = 
[
    "7a4a81ba14f4b32b3c7f061f718cc15b0ca3ab76196a7e9e62f70dcc0add10092906a237d27b034f541731df41fee38d2f4b5d8998678b7d06cad2532fbecc13",
    "6edfa6a70925257e952f161d941092bf30cd70d1f27a6cba53affbe9de7a450dfc02b5c500fbeb9ca2443df0a941269ad65c8bede560f033ae802d6499281a9e",
    "492a5db1fdf4f133e4d8e92c0637ae13a8da6f19bc93da4ed6b0736d8bc602063a229846a43e18b8ba39de1cdba274a11fcda3cf482e3ca908cdf3c3f7ffdb04",
    "8396e4d78a12580388c919b8a201036d48a1470c9684eb653ea89f458927730bc32e8f91763dfce581ff60125664cd6e1a20ed684677a6d8dccbb6b23c3a0182"
]

let vv5 = 
[

    "16ac0490dc79eef786f88dc09c3a8fed15d371e745bdadd9c1f7a08b4d37d50aea565d857cad2b0706642f58caa09fc1073848491994f690b79e8076beca1018",
    "cad2ef53a9dfe02a92606a34f40868fd746209a4ede0c790e0d3ec5e4c90021019b65c7550174fc117aed8d0aa619b94ddd1c08b7a573b9810490e3bbc36ba01",
    "d88949fae2178d3e601abd22f47f64dfd9f8a532d6abd57b3957f0197480cc1b6effc65b04b475279c8de4c585ad8ba518cf35df406fdca8e5f51d5a6918360b",
    "019c9a96b62afb4210616244cdc42936b503814762132658a20c283b9dfe3d005c1c7415903ed953c71539961e911602c8a272d41d5d08184a5f269ee08a941e"
    
]

let vv6 = 
[
    "76e014c5dc7a2ae9d0271936ecac410bee3baf9b88b64ad6a166a1c2aab0991992fec4dc4a80447cb795fa07cd77cdce1c5680710ba016a041c6db4a782f368d",
    "117b180362500cdbd655d3bb0f97b22b57521302938ce5a34e4bcf3c2988ad10688116b0bedd6215aae314bfb240158834b87bd828c01454916d5066d1241691",
    "2c811310b69511e286bd898a6d0f5a306594357e4faee25e69b76cc21b74e5118d96a26e59263dbfe7819be40d7f404d57dca16c9c17ba3c0f95cd531c133593",
    "93d15991f4f0269886cc3e1b99bf2b79bba44ecdc00891bc4b8e6195e1b14e1557ddf84001ef06d5cc0a3f19502f320bfc6f7e11ed75eb21731f68629897461b"
]

let groupPub = module.deriveGroupPubTBLS([vv1,vv2,vv3,vv4,vv5,vv6])

console.log(groupPub) //8aae5ae3b51a6f4bba62f64ab44b2135339831f662f8ef9e004bffb1458faa045f2c9a640acb466c5c35e2c9af757ac7fad74e3865b8527452619236822f9797

```



### <b>Етап 4 - перевірка шар</b>

Чи можемо ми тепер закидувати кошти на гаманець <code>8aae5ae3b51a6f4bba62f64ab44b2135339831f662f8ef9e004bffb1458faa045f2c9a640acb466c5c35e2c9af757ac7fad74e3865b8527452619236822f9797</code> і бути впевненими що все ОК? Ні, бо треба спершу перевірити валідність шар. Це є елементом VSS - ми повинні впевнитися у тому, що кожен з друзів надіслав нам валідну шару. Для цього в модулі існує функція <code>verifyShareTBLS()</code>

Розглянемо її параметри

<ul>
    <li>
    <code>hexMyId</code> - це наш ID отриманий на кроці 1
    </li>
    <li>
    <code>hexSomeSignerSecretKeyContribution</code> - це шара яку ми отримуємо від деякого друга.
    </li>
    <li>
    <code>hexSomeSignerVerificationVector</code> - це вектор верифікації який отримав той друг який прислав нам шару
    </li>
</ul>

Розглянемо процес верифікації. Отже, ми друг 1 і маємо наступне

```js

/*

Вектор верифікації

[
    
    "a7e972de2362e725831efdebe080842f9ee275915a718dea43e561fa0d1af41b3192f849e1128d69f62018b9e7a9387f8b8611488a72480305991d480dad0e11"
    "0003e06c68ba56e45db0ee64a06a6935554e0b7f4849da26223cf831cdfd341bf9fe628590eb436197145206eb8f2eb0baef673eff23f63eaa6fe74e59da7c05"
    "53ad69e15e782a60fe63ee07d9e768dff002dfc54fa6fd6e4ea4531fc00d2700472fb244c9fa740ec354e417e8bd241c875caf29c4abad251a042d7becca4d15"
    "21b090515bf15d9ec6c4f4dd4df5f49a64f21e9b9c268b91b09bd77adcf2d517303fe3cf3bdd710ab783ae413476f7ccd050c1be02524a42484243b47767b71f"
    
]


Набір шар

[
    "79b359142147dd33383e32ac2476577d7760245a644fdad8730b22fad5726b0d"      залишається у нас
    "2ba86e244fe8ad6ea665598257e1a8700b433a2e5bd967116fcf982b9db0df05"      йде до друга 2
    "af5f947ef9db70f50a8a5a5ee07170e78974c641b52942681a229605ff860e1f"      йде до друга 3
    "0f0389cb5fdc97c14d3c494ada022bafb78988efb891258b9c487b9475febd0a"      йде до друга 4
    "c8472e66991a8a1121a5f89f3ae5c1200455f77da838b965f8b9ef5af0701e07"      йде до друга 5
    "565a252bc870bdcf046fb76a25c45c56cab9d4a5624e794319dd13dbc7e9781f"      йде до друга 6
]

ID = 4bf5122f344554c53bde2ebb8cd2b7e3d1600ad631c385a5d7cce23c7785451a

*/

```


Уявімо, що ми відправляємо шару другу 4. Таким чином, друг 4 матиме наступне


```js

/*

Вектор верифікації

[
    "7a4a81ba14f4b32b3c7f061f718cc15b0ca3ab76196a7e9e62f70dcc0add10092906a237d27b034f541731df41fee38d2f4b5d8998678b7d06cad2532fbecc13"
    "6edfa6a70925257e952f161d941092bf30cd70d1f27a6cba53affbe9de7a450dfc02b5c500fbeb9ca2443df0a941269ad65c8bede560f033ae802d6499281a9e"
    "492a5db1fdf4f133e4d8e92c0637ae13a8da6f19bc93da4ed6b0736d8bc602063a229846a43e18b8ba39de1cdba274a11fcda3cf482e3ca908cdf3c3f7ffdb04"
    "8396e4d78a12580388c919b8a201036d48a1470c9684eb653ea89f458927730bc32e8f91763dfce581ff60125664cd6e1a20ed684677a6d8dccbb6b23c3a0182"
]

Набір шар

[

    89b3400f482b4bed1e1eba002df97c8a7933c435fdd990d043fd527255010406        йде до друга 1
    36ae6a22edc96b63ae6bf5ca78445f62117d2aeb0606876b5b0cd321c5338b0a        йде до друга 2
    2170ec403427f94086f4e4712aef6e0b719f58160d8acdd27e26eaaa95676e1a        йде до друга 3
    88e7e3aea7506953618a4f6c3b09e52fa7e1f881f16c70b66ef67c2ba80c5c0e        залишається у нас
    3569f445bbc8274318a0a3e761c016610bcb8b2cb414b49a76979181f9a3480f        йде до друга 5
    b7a2bc301f42f0aa7acff13015b5c28ddd13d8b3fee5f3f81978463bba5efb16        йде до друга 6

]

ID=e52d9c508c502347344d8c07ad91cbd6068afc75ff6292f062a09ca381c89e11

++++++++++++++++++++++++++++++++++++++++++++++++ Отримав шару від нас ++++++++++++++++++++++++++++++++++++++++++++++++

SHARE_FROM_FRIEND_1 = 0f0389cb5fdc97c14d3c494ada022bafb78988efb891258b9c487b9475febd0a

VERIFICATION_VECTOR_OF_FRIEND_1 = [
    
    "a7e972de2362e725831efdebe080842f9ee275915a718dea43e561fa0d1af41b3192f849e1128d69f62018b9e7a9387f8b8611488a72480305991d480dad0e11"
    "0003e06c68ba56e45db0ee64a06a6935554e0b7f4849da26223cf831cdfd341bf9fe628590eb436197145206eb8f2eb0baef673eff23f63eaa6fe74e59da7c05"
    "53ad69e15e782a60fe63ee07d9e768dff002dfc54fa6fd6e4ea4531fc00d2700472fb244c9fa740ec354e417e8bd241c875caf29c4abad251a042d7becca4d15"
    "21b090515bf15d9ec6c4f4dd4df5f49a64f21e9b9c268b91b09bd77adcf2d517303fe3cf3bdd710ab783ae413476f7ccd050c1be02524a42484243b47767b71f"
    
]

*/

```

Ок, давайте здійснимо перевірку


```js

let SHARE_FROM_FRIEND_1 = "0f0389cb5fdc97c14d3c494ada022bafb78988efb891258b9c487b9475febd0a"

let VERIFICATION_VECTOR_OF_FRIEND_1 = [
    
    "a7e972de2362e725831efdebe080842f9ee275915a718dea43e561fa0d1af41b3192f849e1128d69f62018b9e7a9387f8b8611488a72480305991d480dad0e11",
    "0003e06c68ba56e45db0ee64a06a6935554e0b7f4849da26223cf831cdfd341bf9fe628590eb436197145206eb8f2eb0baef673eff23f63eaa6fe74e59da7c05",
    "53ad69e15e782a60fe63ee07d9e768dff002dfc54fa6fd6e4ea4531fc00d2700472fb244c9fa740ec354e417e8bd241c875caf29c4abad251a042d7becca4d15",
    "21b090515bf15d9ec6c4f4dd4df5f49a64f21e9b9c268b91b09bd77adcf2d517303fe3cf3bdd710ab783ae413476f7ccd050c1be02524a42484243b47767b71f"
    
]

let ID_OF_FRIEND_4 = "e52d9c508c502347344d8c07ad91cbd6068afc75ff6292f062a09ca381c89e11"

let isShareOK = module.verifyShareTBLS(ID_OF_FRIEND_4,SHARE_FROM_FRIEND_1,VERIFICATION_VECTOR_OF_FRIEND_1) //true

```

<div align="center">

## Аналогічну процедуру проводить кожний з друзів на кожній шарі !

</div>


Тільки за умови якщо ніяких порушень немає - друзі можуть безпечно закидувати валюту на гаманець <code>8aae5ae3b51a6f4bba62f64ab44b2135339831f662f8ef9e004bffb1458faa045f2c9a640acb466c5c35e2c9af757ac7fad74e3865b8527452619236822f9797</code>. І зверніть увагу, в блокчейні буде лише цей публічний ключ - без будь-яких додаткових налаштувань чи умов. Аналогічний розмір ключа буде навіть за умови 20 чи 100 учасників-підписантів.



### <b>Етап 5 - генерація часткових підписів</b>

Ок, перейдемо до моменту коли друзі врешті вирішили здійснити покупку. Найгірший з можливих варіантів - це коли 4 із 6 друзів погоджуються з покупкою, а 2 з них - ні. Тим не менш, 4 - це достатній поріг і кожному з 4 друзів треба згенерувати частковий підпис, для того, щоб потім їх об'єднати і відправити в блокчейн як криптографічний доказ того, що поріг досягнутий і мережа може дозволити витрачати кошти на цьому гаманці. Для цього, кожний з 4 друзів має згенерувати частковий підпис і поділитися ним з трьома іншими друзями.

> Варто зауважити, що поріг <b>T</b> вказує на мінімальне значення підписантів. В нашому прикладі ми розглядаємо саме такий випадок. Однак, кошти можна буде витратити навіть коли 5 або всі 6 згодні з рішенням

Так от, для генерації часткового підпису використаємо функцію <code>signTBLS()</code>. Розглянемо її параметри:

<ul>
    <li>
    <code>hexMyId</code> - це наш ID отриманий на кроці 1
    </li>
    <li>
    <code>sharedPayload</code> - це масив такого виду

```js

sharedPayload:[
            
    {
        verificationVector://Вектор верифікації отриманий від підписанта_1 у hex форматі
        secretKeyShare://Шара яку ми отримали від підписанта_1 у hex форматі
            
    },
            
    {
        verificationVector://Вектор верифікації отриманий від підписанта_2 у hex форматі
        secretKeyShare://Шара яку ми отримали від підписанта_2 у hex форматі
            
    },
            
    ...,
    {
                
        verificationVector://Вектор верифікації отриманий від підписанта_N у hex форматі
        secretKeyShare://Шара яку ми отримали від підписанта_N у hex форматі

    }

]

```
</li>
    <li>
    <code>message</code> - це повідомлення <code>M</code> яке треба підписати
    </li>
</ul>

Давайте згенеруємо підписи для перших 4 друзів

```js

//------------------------------- ДРУГ 1 -------------------------------


let vv1=[
    
    "a7e972de2362e725831efdebe080842f9ee275915a718dea43e561fa0d1af41b3192f849e1128d69f62018b9e7a9387f8b8611488a72480305991d480dad0e11",
    "0003e06c68ba56e45db0ee64a06a6935554e0b7f4849da26223cf831cdfd341bf9fe628590eb436197145206eb8f2eb0baef673eff23f63eaa6fe74e59da7c05",
    "53ad69e15e782a60fe63ee07d9e768dff002dfc54fa6fd6e4ea4531fc00d2700472fb244c9fa740ec354e417e8bd241c875caf29c4abad251a042d7becca4d15",
    "21b090515bf15d9ec6c4f4dd4df5f49a64f21e9b9c268b91b09bd77adcf2d517303fe3cf3bdd710ab783ae413476f7ccd050c1be02524a42484243b47767b71f",

]

let sharesOf1=[

    "79b359142147dd33383e32ac2476577d7760245a644fdad8730b22fad5726b0d",
    "2ba86e244fe8ad6ea665598257e1a8700b433a2e5bd967116fcf982b9db0df05",
    "af5f947ef9db70f50a8a5a5ee07170e78974c641b52942681a229605ff860e1f",
    "0f0389cb5fdc97c14d3c494ada022bafb78988efb891258b9c487b9475febd0a",
    "c8472e66991a8a1121a5f89f3ae5c1200455f77da838b965f8b9ef5af0701e07",
    "565a252bc870bdcf046fb76a25c45c56cab9d4a5624e794319dd13dbc7e9781f"

]

let ID_1 = '4bf5122f344554c53bde2ebb8cd2b7e3d1600ad631c385a5d7cce23c7785451a'


//------------------------------- ДРУГ 2 -------------------------------

let vv2=[
    
    "6f4c90be986832a71aa5b8d25007d27adc3aa267acdca189a42b9a17efc8dd17bcde25c173d18eeba5bae067e52cc5ba16fd3099af04804c8a40b79ac595c617",
    "775449cde76ce6333c53317e6b6a1eb47ee87af2bc56548bb38fea5da266dd0704ae043c288f9fb792b870128738ad7360454724325b5260bb45e332a3d51221",
    "dd5a9d1f3236dd7e091dcb4272fb3cec624c22a0e59de283ce345603850e180e263806875158263b2bd46f0b7af4d78596e43008948431b07d174b6c8d6da388",
    "8176bdd4610dd21cdb6bfb9b526a9e3ee2804c57ac9b9094235a4c1947c2f20aa1282cc8dd5e2ec09183c677920106024aab0bd9a2ecaa5539ff9e1c4025d1a3"
]

let sharesOf2=[

    '173fc58cb61188a8fad233a689baec3db34657c3bb8536b21cbad70db463130b',
    '834ffe5b7794a5c8666210a98beaba4a48d344dd2e691ce4edb2196f6b2da317',
    'a8e50fdc4d6f1f20047b7297ff54ac2d807cbfb6cd12e8f499e6eaf7b5feb010',
    '50b2c925a3463136414715d0f99c594c1aa2d7e5857d7881f62dcdc7676c191a',
    'd7f329f64448ddd56350a2a3a45d944679a2c4fc76ecb3770d760592b08c8b0a',
    '2ddedf940ea38c9809735793cec9ded6d0d6e573181611c0f3efa604b6b92e1c'

]


let ID_2 = 'dbc1b4c900ffe48d575b5da5c638040125f65db0fe3e24494b76ea986457d906'


//------------------------------- ДРУГ 3 -------------------------------

let vv3=[

    "586797df712ba4dcb15e78a36e6ef4a3f9ba1b05ce3a6a9001f0cdfefbd8f8080a33a3182a9204657594ebc6749656166f119ab2a81515266f62ce10e327ca09",
    "62b9b08eadbbb20ef840923c89ac53b73aa31fa3e493097f49496693a795131b46dba854f76bfaf369beb77a7a80e2fe66525b16341a88022e1dc635bd7dc385",
    "2577181e0384bc99ceb066acf69323e236160796df37f6e688d3274e4e026a03997460021b39da01a8a1465566e08f6a455333742fb6646f11beeab2a8dd2420",
    "b2a2ace162f22b200c7aa807083bae80e69214dcdaa9708c66f81d1e8d4a0c18c3b459214292bd683c9b80809ea3e7e95288656caf0b34ec260b820a79b08520"
]

let sharesOf3=[

    '82858da32c368bc8dea20ab9646d87f84eb478e51a45a55394710c96c71a080f',
    '6a12a6abfe939015aca70e67c96e9768ad9af0e6541c62e82b51ca32176c8816',
    'b607ae0d7be4bbbad607967c40841e67f8b992e15a44fc3d0dbc5a14a16f9b0e',
    '73d57dfb5cd773a19a34052184d1f0b2576bc127ee1d92ae596c51bf3a6e2c24',
    'd9dc10623e261047a0ac924595c356f5d9b192ab28f29276bf5c21206c143902',
    '952e03949438a001aafb2b1e864635c25b229047fc85b3178dd1d049ecbdcd04'

]


let ID_3 = '084fed08b978af4d7d196a7446a86b58009e636b611db16211b65a9aadff2905'

//------------------------------- ДРУГ 4 -------------------------------

let vv4=[

    "7a4a81ba14f4b32b3c7f061f718cc15b0ca3ab76196a7e9e62f70dcc0add10092906a237d27b034f541731df41fee38d2f4b5d8998678b7d06cad2532fbecc13",
    "6edfa6a70925257e952f161d941092bf30cd70d1f27a6cba53affbe9de7a450dfc02b5c500fbeb9ca2443df0a941269ad65c8bede560f033ae802d6499281a9e",
    "492a5db1fdf4f133e4d8e92c0637ae13a8da6f19bc93da4ed6b0736d8bc602063a229846a43e18b8ba39de1cdba274a11fcda3cf482e3ca908cdf3c3f7ffdb04",
    "8396e4d78a12580388c919b8a201036d48a1470c9684eb653ea89f458927730bc32e8f91763dfce581ff60125664cd6e1a20ed684677a6d8dccbb6b23c3a0182"
]

let sharesOf4=[

    '89b3400f482b4bed1e1eba002df97c8a7933c435fdd990d043fd527255010406',
    '36ae6a22edc96b63ae6bf5ca78445f62117d2aeb0606876b5b0cd321c5338b0a',
    '2170ec403427f94086f4e4712aef6e0b719f58160d8acdd27e26eaaa95676e1a',
    '88e7e3aea7506953618a4f6c3b09e52fa7e1f881f16c70b66ef67c2ba80c5c0e',
    '3569f445bbc8274318a0a3e761c016610bcb8b2cb414b49a76979181f9a3480f',
    'b7a2bc301f42f0aa7acff13015b5c28ddd13d8b3fee5f3f81978463bba5efb16'

]

let ID_4 = 'e52d9c508c502347344d8c07ad91cbd6068afc75ff6292f062a09ca381c89e11'


//------------------------------- ДРУГ 5 -------------------------------


let vv5=[

    "16ac0490dc79eef786f88dc09c3a8fed15d371e745bdadd9c1f7a08b4d37d50aea565d857cad2b0706642f58caa09fc1073848491994f690b79e8076beca1018",
    "cad2ef53a9dfe02a92606a34f40868fd746209a4ede0c790e0d3ec5e4c90021019b65c7550174fc117aed8d0aa619b94ddd1c08b7a573b9810490e3bbc36ba01",
    "d88949fae2178d3e601abd22f47f64dfd9f8a532d6abd57b3957f0197480cc1b6effc65b04b475279c8de4c585ad8ba518cf35df406fdca8e5f51d5a6918360b",
    "019c9a96b62afb4210616244cdc42936b503814762132658a20c283b9dfe3d005c1c7415903ed953c71539961e911602c8a272d41d5d08184a5f269ee08a941e"
]


let sharesOf5=[

    'f441e4786e7501271994d3819ff2997456c281c8de7225f856165fdf61d1270c',
    '3846038253b8099f82f8fa23632a2f41d6dbf1b41a579e59115f3a3d44a9d218',
    'fbffb23b9b08052c57b157908f8eddc61ddec2810d984e79114fac3db8f78017',
    'ba0d2e862b32e36e92a879519bc75a478305a26d292e69c00c5caaa00b4a6f1d',
    'f1271c0a82ef0173a8584b61b9991428ed5aa4e1052318827cd13bce3e076b1b',
    '455c84ebeb83ca3ef8c71d09bc310083848ca694244997dbbff4a74af3745a09'

]

let ID_5='e77b9a9ae9e30b0dbdb6f510a264ef9de781501d7b6b92ae89eb059c5ab7431b'



//------------------------------- ДРУГ 6 -------------------------------


let vv6=[

    "76e014c5dc7a2ae9d0271936ecac410bee3baf9b88b64ad6a166a1c2aab0991992fec4dc4a80447cb795fa07cd77cdce1c5680710ba016a041c6db4a782f368d",
    "117b180362500cdbd655d3bb0f97b22b57521302938ce5a34e4bcf3c2988ad10688116b0bedd6215aae314bfb240158834b87bd828c01454916d5066d1241691",
    "2c811310b69511e286bd898a6d0f5a306594357e4faee25e69b76cc21b74e5118d96a26e59263dbfe7819be40d7f404d57dca16c9c17ba3c0f95cd531c133593",
    "93d15991f4f0269886cc3e1b99bf2b79bba44ecdc00891bc4b8e6195e1b14e1557ddf84001ef06d5cc0a3f19502f320bfc6f7e11ed75eb21731f68629897461b"
]

let sharesOf6=[

    'bbf907b9028b87ba335e274f0cbcad8286ea57361788859f6392aef9ea5eb124',
    'f7e2749b4803c55d42d4487916569510e50350d0bb5ec1879d8d00135cc7ae00',
    '1640c3244249f9b215a23015c5ac77bf27a1e9c39b46ca15908646742066be08',
    'aee5fb00cfaedb98c1a6846c3a25c183b57ae1938b0de18fd37503b93b982300',
    '2c84ae3f4fd9d4b8eb3c60e26b248086ff66892cda1a4fa5834cedfebee5cc0c',
    '00f055619d846105cb5de592f1b4f9b209fd8db6b29bf22fd6859dac91102322'

]

let ID_6='67586e98fad27da0b9968bc039a1ef34c939b9b8e523a8bef89d478608c5ec16'




//------------------------------------------------ Процес генерації часткових підписів ------------------------------------------------


// Тут достатньо лише для 4 друзів

let sharedPayloadFor1 = [

    {
        verificationVector:vv1
        secretKeyShare:sharesOf1[0]
    },
    {
        verificationVector:vv2
        secretKeyShare:sharesOf2[0]
    },
    {
        verificationVector:vv3
        secretKeyShare:sharesOf3[0]
    },
    {
        verificationVector:vv4
        secretKeyShare:sharesOf4[0]
    },
    {
        verificationVector:vv5
        secretKeyShare:sharesOf5[0]
    },
    {
        verificationVector:vv6
        secretKeyShare:sharesOf6[0]
    }

]


let sharedPayloadFor2 = [

    {
        verificationVector:vv1
        secretKeyShare:sharesOf1[1]
    },
    {
        verificationVector:vv2
        secretKeyShare:sharesOf2[1]
    },
    {
        verificationVector:vv3
        secretKeyShare:sharesOf3[1]
    },
    {
        verificationVector:vv4
        secretKeyShare:sharesOf4[1]
    },
    {
        verificationVector:vv5
        secretKeyShare:sharesOf5[1]
    },
    {
        verificationVector:vv6
        secretKeyShare:sharesOf6[1]
    }

]


let sharedPayloadFor3 = [

    {
        verificationVector:vv1
        secretKeyShare:sharesOf1[2]
    },
    {
        verificationVector:vv2
        secretKeyShare:sharesOf2[2]
    },
    {
        verificationVector:vv3
        secretKeyShare:sharesOf3[2]
    },
    {
        verificationVector:vv4
        secretKeyShare:sharesOf4[2]
    },
    {
        verificationVector:vv5
        secretKeyShare:sharesOf5[2]
    },
    {
        verificationVector:vv6
        secretKeyShare:sharesOf6[2]
    }

]



let sharedPayloadFor4 = [

    {
        verificationVector:vv1
        secretKeyShare:sharesOf1[3]
    },
    {
        verificationVector:vv2
        secretKeyShare:sharesOf2[3]
    },
    {
        verificationVector:vv3
        secretKeyShare:sharesOf3[3]
    },
    {
        verificationVector:vv4
        secretKeyShare:sharesOf4[3]
    },
    {
        verificationVector:vv5
        secretKeyShare:sharesOf5[3]
    },
    {
        verificationVector:vv6
        secretKeyShare:sharesOf6[3]
    }

]


// Нехай наше повідомлення буде згодою друзів про покупку намету для походу за 300$
let message = 'WE BUY A TENT FOR 300$'


//------------------------------------------ А ось і часткові підписи ------------------------------------------

let sigShare1 = module.signTBLS(ID_1,sharedPayloadFor1,message) //{"sigShare":"fe789c95b112099b370d0dfdb7aae4fb80087aa2cea0334266de5792ee27d617","id":"4bf5122f344554c53bde2ebb8cd2b7e3d1600ad631c385a5d7cce23c7785451a"}

let sigShare2 = module.signTBLS(ID_2,sharedPayloadFor2,message) //{"sigShare":"3cafeb0275cea62faf29df46c3d7bb52e1a6d33ab89aa0ad53d5bf1f53a0e285","id":"dbc1b4c900ffe48d575b5da5c638040125f65db0fe3e24494b76ea986457d906"}

let sigShare3 = module.signTBLS(ID_3,sharedPayloadFor3,message) //{"sigShare":"14a7a88ad49586e8c5a4e2c87b91b82f005fc595e60bd39b3a8d1cc3d20c9815","id":"084fed08b978af4d7d196a7446a86b58009e636b611db16211b65a9aadff2905"}

let sigShare4 = module.signTBLS(ID_4,sharedPayloadFor4,message) //{"sigShare":"6b46897a312f2e9e8f515f359f2adeb50c5c9fb5f6317c535a9a351c3e5f6e8a","id":"e52d9c508c502347344d8c07ad91cbd6068afc75ff6292f062a09ca381c89e11"}

```


### <b>Етап 6 - агрегація часткових підписів</b>

Тепер, маючи 4 часткові підписи можемо отримати <code>MasterSig</code>. Маючи повідомлення <code>M</code>(WE BUY A TENT FOR 300$) та наш публічний <code>MasterPub</code>(8aae5ae3b51a6f4bba62f64ab44b2135339831f662f8ef9e004bffb1458faa045f2c9a640acb466c5c35e2c9af757ac7fad74e3865b8527452619236822f9797) можна буде перевірити цей <code>MasterSig</code> та дозволити операцію з коштами.


Настав час функції <code>buildSignature()</code>. Традиційно, розглянемо параметри

<ul>

<li>

<code>signaturesArray</code> - масив виду

```js

[ {sigShare:signedShareA,id:hexIdA}, {sigShare:signedShareB,id:hexIdB},... {sigShare:signedShareX,id:hexIdX} ]

//  [+] sigShareN - частковий підпис від учасника N в hex форматі
//  [+] id - ID цього учасника

```

</li>

</ul>


Виконаємо функцію


```js

let sigShareAndId1={sigShare:"fe789c95b112099b370d0dfdb7aae4fb80087aa2cea0334266de5792ee27d617",id:"4bf5122f344554c53bde2ebb8cd2b7e3d1600ad631c385a5d7cce23c7785451a"}

let sigShareAndId2={sigShare:"3cafeb0275cea62faf29df46c3d7bb52e1a6d33ab89aa0ad53d5bf1f53a0e285",id:"dbc1b4c900ffe48d575b5da5c638040125f65db0fe3e24494b76ea986457d906"}

let sigShareAndId3={sigShare:"14a7a88ad49586e8c5a4e2c87b91b82f005fc595e60bd39b3a8d1cc3d20c9815",id:"084fed08b978af4d7d196a7446a86b58009e636b611db16211b65a9aadff2905"}

let sigShareAndId4={sigShare:"6b46897a312f2e9e8f515f359f2adeb50c5c9fb5f6317c535a9a351c3e5f6e8a",id:"e52d9c508c502347344d8c07ad91cbd6068afc75ff6292f062a09ca381c89e11"}



let masterSignature=module.buildSignature([sigShareAndId1,sigShareAndId2,sigShareAndId3,sigShareAndId4]) //46c1f011e7d7039bb77a638e60e7e1c3acbfb3124ec2b06b918f1fd5d4a0b39c

```

Отримали <code>MasterSig</code> зі значенням <code>46c1f011e7d7039bb77a638e60e7e1c3acbfb3124ec2b06b918f1fd5d4a0b39c</code>.На цьому етапі можна порівняти переваги порогових підписів та звичайних мультипідписів. Якби ми користувались мультипідписами і мали б таку ситуацію, коли 4/6, то ось що потрібно було б публікувати в блокчейн(дивись порівняльну таблицю):

</br>

<table align="center">

<tr>
<td><b>Мультипідписи</b></td>
<td><b>Порогові підписи</b></td>
</tr>

<tr>
<td>48-байтовий агрегований BLS ключ який вміщає в себе 4 публічні ключі друзів</td>
<td>32-байтовий агрегований TBLS підпис</td>
</tr>

<tr>
<td> 2*48=96 байт. Це додаткові N байт які лінійно залежать від кількості тих, хто не підписував повідомлення.</br>
Вони та агреговані 4 ключа поєднуються та порівнюються з <code>MasterPub</code> на рівність</td>
</tr>

<tr>
<td>96-байтовий агрегований BLS підпис 4 учасників. Валідність підпису можна перевірити ключем, що включає в себе публічні ключі 4 учасників</td>
</tr>

</table>

<br/>

> За допомогою цієї таблиці вже видно значні переваги. Оскільки вага доказу для мультипідписів зростає лінійно, то її складність <code>O(N)</code></br>
> Натомість, TBLS завжди стабільна і займає 32 байти - тобто <code>O(1)</code>.

### <b>Етап 7 - перевірка групових підписів за допомогою групового публічного ключа</b>

Отже, на попередньому етапі був отриманий підпис групи. Ще раніше, ми отримали публічний ключ групи - це наша адреса в мережі криптовалюти - <code>8aae5ae3b51a6f4bba62f64ab44b2135339831f662f8ef9e004bffb1458faa045f2c9a640acb466c5c35e2c9af757ac7fad74e3865b8527452619236822f9797</code>

Все що потрібно мережі зробити аби прийняти транзакцію від друзів - це просто зробити таку перевірку

```js

let masterPub = '8aae5ae3b51a6f4bba62f64ab44b2135339831f662f8ef9e004bffb1458faa045f2c9a640acb466c5c35e2c9af757ac7fad74e3865b8527452619236822f9797'

let masterSig = '46c1f011e7d7039bb77a638e60e7e1c3acbfb3124ec2b06b918f1fd5d4a0b39c'

let message = 'WE BUY A TENT FOR 300$'

let isOk = module.verifyTBLS(masterPub,masterSig,message) //true

```

<div align="center">

# 🔥Вітаємо - тепер ви навчилися тому, як використовувати TBLS в своїх рішеннях🔥


</div>